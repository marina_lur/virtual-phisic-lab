CREATE USER system WITH password 'system';

 ALTER USER system WITH SUPERUSER;

GRANT ALL PRIVILEGES ON DATABASE vpl_database TO system;

CREATE SEQUENCE users_user_id_seq;
CREATE SEQUENCE groups_group_id_seq;
CREATE SEQUENCE departments_department_id_seq;
CREATE SEQUENCE positions_position_id_seq;
CREATE SEQUENCE tests_test_id_seq;
CREATE SEQUENCE questions_question_id_seq;
CREATE SEQUENCE labs_lab_id_seq;
CREATE SEQUENCE answers_answer_id_seq;
CREATE SEQUENCE users_tests_id_seq;
CREATE SEQUENCE users_questions_id_seq;
CREATE SEQUENCE tests_questions_id_seq;
CREATE SEQUENCE users_labs_id_seq;


CREATE TABLE departments
(
  department_id INTEGER NOT NULL PRIMARY KEY DEFAULT nextval('departments_department_id_seq'),
  department_name VARCHAR(200)
);

CREATE TABLE groups
(
  group_id INTEGER NOT NULL PRIMARY KEY DEFAULT nextval('groups_group_id_seq'),
  group_name TEXT NOT NULL,
  department_id INTEGER,
  CONSTRAINT fk_department_id FOREIGN KEY (department_id) REFERENCES departments (department_id)
);



CREATE TABLE positions
(
  position_id INTEGER NOT NULL PRIMARY KEY DEFAULT nextval('positions_position_id_seq'),
  position_name  VARCHAR(50)
);

CREATE TABLE users
(
  user_id       INTEGER NOT NULL PRIMARY KEY DEFAULT nextval('users_user_id_seq'),
  name          VARCHAR(100),
  surname       VARCHAR(100),
  middlename    VARCHAR(100),
  role          VARCHAR(50),
  group_id      INTEGER,
  department_id INTEGER,
  position_id   INTEGER,
  login         VARCHAR(200),
  hash          TEXT,
  salt          TEXT,
  CONSTRAINT fk_department_id FOREIGN KEY (department_id) REFERENCES departments (department_id),
  CONSTRAINT fk_group_id FOREIGN KEY (group_id) REFERENCES groups (group_id),
  CONSTRAINT fk_position_id FOREIGN KEY (position_id) REFERENCES positions (position_id)

);

CREATE TABLE tests
(
  test_id INTEGER NOT NULL PRIMARY KEY DEFAULT nextval('tests_test_id_seq'),
  test_name  VARCHAR(50)
);



CREATE TABLE users_tests
(
  id      INTEGER NOT NULL PRIMARY KEY DEFAULT nextval('users_tests_id_seq'),
  test_id INTEGER NOT NULL,
  user_id INTEGER NOT NULL,
  tmstmp  TIMESTAMP,
  CONSTRAINT fk_test_id FOREIGN KEY (test_id) REFERENCES tests (test_id),
  CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES users (user_id)
);

CREATE TABLE questions
(
  question_id INTEGER NOT NULL PRIMARY KEY DEFAULT nextval('questions_question_id_seq'),
  question_text TEXT
);

CREATE TABLE users_questions
(
  id          INTEGER NOT NULL PRIMARY KEY DEFAULT nextval('users_questions_id_seq'),
  question_id INTEGER NOT NULL,
  user_id     INTEGER NOT NULL,
  tmstmp      TIMESTAMP,
  test_id     INTEGER NOT NULL,
  CONSTRAINT fk_question_id FOREIGN KEY (question_id) REFERENCES questions (question_id),
  CONSTRAINT fk_test_id FOREIGN KEY (test_id) REFERENCES tests (test_id),
  CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES users (user_id)
);

CREATE TABLE tests_questions
(
  id          INTEGER NOT NULL PRIMARY KEY DEFAULT nextval('tests_questions_id_seq'),
  question_id INTEGER NOT NULL,
  test_id     INTEGER NOT NULL,
  CONSTRAINT fk_question_id FOREIGN KEY (question_id) REFERENCES questions (question_id),
  CONSTRAINT fk_test_id FOREIGN KEY (test_id) REFERENCES tests (test_id)
);

CREATE TABLE labs
(
  lab_id INTEGER NOT NULL PRIMARY KEY DEFAULT nextval('labs_lab_id_seq'),
  lab_name VARCHAR(200),
  lab_text TEXT,
  lab_json JSONB
);
CREATE TABLE  users_labs
(
  id              INTEGER NOT NULL PRIMARY KEY DEFAULT nextval('users_labs_id_seq'),
  user_id         INTEGER NOT NULL,
  lab_id          INTEGER NOT NULL,
  report_pdf_link TEXT,
  CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES users (user_id),
  CONSTRAINT fk_lab_id FOREIGN KEY (lab_id) REFERENCES labs (lab_id)
);

CREATE TABLE answers
(
  answer_id INTEGER NOT NULL PRIMARY KEY DEFAULT nextval('answers_answer_id_seq'),
  answer_text TEXT,
  question_id INTEGER,
  CONSTRAINT fk_question_id FOREIGN KEY (question_id) REFERENCES questions (question_id)

);

INSERT INTO departments (department_name) VALUES ('Институт информатки');
INSERT INTO groups(group_name,department_id) VALUES ('ПИ-1401',1);
INSERT INTO positions (position_name) VALUES ('студент');
INSERT INTO users(name,surname,middlename,role,group_id,department_id,position_id,login) VALUES ('Марина', 'Евремова','Владиславовна','пользователь', 1,1,1,'nfdfj2');
INSERT INTO users(name,surname,middlename,role,group_id,department_id,position_id,login) VALUES ('Ирина', 'Симакова','Ивановна','пользователь', 1,1,1,'ddss3');
INSERT INTO users(name,surname,middlename,role,group_id,department_id,position_id,login) VALUES ('Артем', 'Петров','Дмитреевич','пользователь', 1,1,1,'fddff4');
INSERT INTO users(name,surname,middlename,role,group_id,department_id,position_id,login) VALUES ('Алина', 'Иванова','Ивановна','пользователь', 1,1,1,'dggs3');
INSERT INTO users(name,surname,middlename,role,group_id,department_id,position_id,login) VALUES ('Дарья', 'Петренко','Станиславовна','пользователь', 1,1,1,'erhh3');
INSERT INTO users(name,surname,middlename,role,group_id,department_id,position_id,login) VALUES ('Дмитрий', 'Парин','Владиромивич','пользователь', 1,1,1,'ddfgdf');
INSERT INTO users(name,surname,middlename,role,group_id,department_id,position_id,login) VALUES ('Екатерина', 'Курова','Викторовна','пользователь', 1,1,1,'fgr3');
