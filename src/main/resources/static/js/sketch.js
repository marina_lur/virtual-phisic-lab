window.requestAnimFrame = (function (callback) {

    return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||

        function (callback) {
            window.setTimeout(callback, 10);
        };
})();



var canvas = document.getElementById('myCanvas');
var context = canvas.getContext('2d');
var doAnim = true;
var step = 38;
var w = canvas.width;
var h = canvas.height;

var background = new Image();
var ob1 = new Image();
var ob2 = new Image();
background.src = "https://1.downloader.disk.yandex.ru/preview/9bd22d1dad564ed527bd5cf85aa1d7f1597cb5668fb68d608de16a70a59c6406/inf/8-G3JUlflXll7gj0pseJM9RZ7fBuYQfiKorIV40BGBTDWBv3Kpaj_HcJiQUOKOGfr7qpNgwhqAcxxYwPKxi9iA%3D%3D?uid=59679827&filename=white_room_stage_dl_down_by_chocosunday-d517wc6.jpg&disposition=inline&hash=&limit=0&content_type=image%2Fjpeg&tknv=v2&size=1280x857";
ob1.src = "https://4.downloader.disk.yandex.ru/preview/b3abc278e5a251b639fc0f24d2ccd875cfb0cc1298d4ea0ef0a842051b22a97b/inf/X2QK5xmJfR3Puop5jebipwmhGKDrMxzy1dNxWzxxd__8BJQ1d0FMAwcxAagOl6clEt7sUS6_NoRRHhPnIGNucg%3D%3D?uid=59679827&filename=%D0%BE%D0%B1%D0%BA%D0%BB1.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&tknv=v2&size=1280x857";
ob2.src = "https://3.downloader.disk.yandex.ru/preview/9a86d212220aeea7f71c0e67da7adf73f6d8eb210e177aa7140435d2f0c283bd/inf/X2QK5xmJfR3Puop5jebip0Yz2Oo8oiRiw32_Hfex6LBETmoI1t5JsPpCkmO91Wua585g7DbkdSy7WdaJm20WJQ%3D%3D?uid=59679827&filename=%D0%BE%D0%B1%D0%BA%D0%BB2.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&tknv=v2&size=1280x857";
var startButton = $("#start");
var refreshButton = $("#refresh");
var stopButton = $("#stop");
var xp = [];
var yp = [];
function canvas_arrow(context, fromx, fromy, tox, toy) {
    var headlen = 10; // length of head in pixels
    var angle = Math.atan2(toy - fromy, tox - fromx);
    context.lineCap = 'round';
    context.moveTo(fromx, fromy);
    context.lineTo(tox, toy);
    context.lineTo(tox - headlen * Math.cos(angle - Math.PI / 6), toy - headlen * Math.sin(angle - Math.PI / 6));
    context.moveTo(tox, toy);
    context.lineTo(tox - headlen * Math.cos(angle + Math.PI / 6), toy - headlen * Math.sin(angle + Math.PI / 6));
// context.strokeStyle = 'black';
    context.stroke();
}
function decorate() {

    context.fillText("q = -1,60*10\u207B\u00B9\u2079" + " Кл", 420, 60);
    context.fillText("m = 9,11*10\u207B\u00B3\u00B9" + " кг", 420, 78);

    context.fillText("y", 250, 70);
    context.fillText("x", w - w / 7, h / 2 + 20);
    context.fillText("7 см", w / 3 + step * 7, h / 2 + 20);
}

function drawLine(x, y, dx, dy, context) {
    context.beginPath();
    context.moveTo(x, y);
    context.lineTo(dx, dy);
    context.strokeStyle = 'black';
    context.stroke();
}
function drawCurve(context) {
    context.strokeStyle = "gray";
    context.moveTo(w / 2, h / 2 - step - 20 - step * 4);
    context.bezierCurveTo(w / 7, h / 4, w / 7, h - h / 4, w / 2, h / 2 + step + 20 + step * 4);

    context.bezierCurveTo(w - w / 7, h - h / 4, w - w / 7, h / 4, w / 2, h / 2 - step - 20 - step * 4);

    context.bezierCurveTo(1200, h / 4, 1200, h - h / 4, w / 2, h / 2 + step + 20 + step * 4);
    context.bezierCurveTo(-400, h - h / 4, -400, h / 4, w / 2, h / 2 - step - 20 - step * 4);
    context.bezierCurveTo(900, h / 4, 900, h - h / 4, w / 2, h / 2 + step + 20 + step * 4);
    context.bezierCurveTo(-100, h - h / 4, -100, h / 4, w / 2, h / 2 - step - 20 - step * 4);
    context.bezierCurveTo(200, h / 4, 200, h - h / 4, w / 2, h / 2 + step + 20 + step * 4);
    context.bezierCurveTo(w - 200, h - h / 4, w - 200, h / 4, w / 2, h / 2 - step - 20 - step * 4);

    canvas_arrow(context, 775, h - h / 2 + 1, 775, h - h / 2);
    canvas_arrow(context, 614, h - h / 2 + 1, 614, h - h / 2);
    canvas_arrow(context, 186, h / 2 + 1, 186, h / 2);
    canvas_arrow(context, 25, h / 2 + 1, 25, h / 2);
    canvas_arrow(context, w - 250, h / 2 + 1, w - 250, h / 2);
    canvas_arrow(context, 250, h / 2 + 1, 250, h / 2);

    context.stroke();

}
function drawCircle(circle, context, color) {
    context.beginPath();
    context.arc(circle.x, circle.y, circle.radius, 0, 2 * Math.PI, false);
    context.fillStyle = color;
    context.fill();
    // context.strokeStyle = '#003300';
    context.stroke();
}

function animateCharges(particle, canvas, context, i) {
    var vx = $("#v0x").val();
    var E = $("#e").val();
    var a = -1.76 * Math.pow(10, 11) * E * 1000;
    var v = vx * Math.pow(10, 6);



    i = i + 0.00000000001;


    if ((particle.y <= h / 2 + step - particle.radius) && (particle.x <= w - step * 7 + particle.radius)) {
        xp.push(particle.x);
        yp.push(particle.y);

        particle.x = (v * i) * 100 / 37.795 + (particle.x - w / 3) + w / 3;
        particle.y = h / 2 + (particle.y - h / 2) - (((a * Math.pow(i, 2)) / 2)) * 100 / 37.95;


    } else if (particle.x >= w - step * 7 + particle.radius) {
        var aq = 0.077;
        var d = 0.02;
        var l = 0.07;
        var OA = d / 2 + aq;
        b = 0.0005;
        var OD = d / 2 + b;
        var q = Math.pow(7.4, -10);
        var AD = Math.sqrt(OA * OA + OD * OD);
        var AA = OA * 2;
        //var Eplus =
    } else {
        doAnim = false;
    }

    context.clearRect(0, 0, canvas.width, canvas.height);
    context.drawImage(background, 0, 0);
    context.drawImage(ob1, w / 3, h / 2 + step);
    context.drawImage(ob2, w / 3, h / 2 - step - 20);
    context.font = "18px Times";


    drawLine(w / 9, h / 2, w - w / 9, h / 2, context);
    drawLine(w / 3, h / 9, w / 3, h - h / 6, context);
    drawLine(w / 3 + step * 7, h / 2 - 3, w / 3 + step * 7, h / 2 + 3, context);


    timeShow = Number((((particle.x - (w / 3)) / step) / v) * Math.pow(10, 6)).toFixed(2);


    drawCurve(context);
    //  context.strokeStyle = "black";
    canvas_arrow(context, w / 3, h / 9 + 1, w / 3, h / 9);
    canvas_arrow(context, w - w / 9, h / 2, w - w / 9, h / 2);

    context.fillStyle = "rgba(255,255,255, 0.5)";
    context.beginPath();
    context.fillRect(0, 240, 200, 150);
    context.beginPath();
    context.fillRect(410, 25, 200, 65);
    context.fillStyle = 'black';
    context.fillText("x= " + Number((particle.x - (w / 3)) / step).toFixed(2) + " см", 20, 270);
    context.fillText("y= " + (-1) * Number((particle.y - (h / 2)) / step).toFixed(2) + " см", 20, 290);
    context.fillText("t= " + timeShow + "*10\u207B\u2078" + " с", 20, 310);
    context.fillText("v\u2093 " + " = " + vx + "*10\u2076" + " м/с", 20, 340);
    context.fillText("vᵧ= " + Number((-a * timeShow) / Math.pow(10, 14)).toFixed(2) + "*10\u2076" + " м/с", 20, 360);

    decorate();
    for (var k = 0; k < xp.length - 1; k++) {
        context.beginPath();
        context.moveTo(xp[k], yp[k]);
        context.lineTo(xp[k + 1], yp[k + 1]);
        context.stroke();
    }
    drawCircle(particle, context, 'black');
    drawCircle(chargeOne, context, 'blue');
    drawCircle(chargeTwo, context, 'red');
    context.fillStyle = 'white';
    context.font = " bold 21px Times";
    context.fillText("-", w / 2 - 3, h / 2 - step - 15 - step * 4);
    context.fillText("+", w / 2 - 6, h / 2 + step + 25 + step * 4);
    context.fillStyle = 'black';


    requestAnimFrame(function () {
        if (doAnim) {

            animateCharges(particle, canvas, context, i);
        } else {
            context = null;
            return;
        }
    });

}


function animateThreads(particle, canvas, context, i, x1) {

    var E = $("#e").val();
    var vx = $("#v0x").val();

    var a = -1.76 * Math.pow(10, 11) * E * 1000;
    var v = vx * Math.pow(10, 6);
    var tS = 0.01 / v;


    i = i + 0.00000000001;
    // console.log(sinAlpha1);


    //console.log(time);
    if ((particle.y <= h / 2 + step - particle.radius) && (particle.x <= w - step * 7 + particle.radius)) {
        xp.push(particle.x);
        yp.push(particle.y);
        particle.x = (v * i) * 100 / 37.795 + (particle.x - w / 3) + w / 3;
        particle.y = h / 2 + (particle.y - h / 2) - (((a * Math.pow(i, 2)) / 2)) * 100 / 37.95;

        //console.log(   particle.x+"  "+ particle.y)
    } else if (particle.x >= w - step * 7 + particle.radius) {
        xp.push(particle.x);
        yp.push(particle.y);
        var initialE = 0;
        var deltaL = 0.0005;
        var x2 = x1 - deltaL;
        var e0 = 8.85 * Math.pow(10, -12);
        var sigma = e0 * E * 1000;
        var deltaX = 0.00007;
        var lambda = deltaX * sigma;

        var r0 = 0.01;
        var sinAlpha1 = x1 / (Math.sqrt(x1 * x1 + r0 * r0));

        var sinalpha2 = x2 / (Math.sqrt(x2 * x2 + r0 * r0));

        var Eres = ((2 * lambda) / (4 * Math.PI * e0 * r0)) * (sinAlpha1 - sinalpha2);
        var aOut = -1.76 * Math.pow(10, 11) * E * 1000 * Eres;

        particle.x = (v * i) * 100 / 37.795 + (particle.x - w / 3) + w / 3;
        particle.y = h / 2 + (particle.y - h / 2) - (((aOut * Math.pow(i, 2)) / 2)) * 100 / 37.95;
        x1 = x1 + 0.0001;



    } else {
        doAnim = false;
    }

    context.clearRect(0, 0, canvas.width, canvas.height);
    context.drawImage(background, 0, 0);
    context.drawImage(ob1, w / 3, h / 2 + step);
    context.drawImage(ob2, w / 3, h / 2 - step - 20);
    context.font = "18px Times";


    drawLine(w / 9, h / 2, w - w / 9, h / 2, context);
    drawLine(w / 3, h / 9, w / 3, h - h / 6, context);
    drawLine(w / 3 + step * 7, h / 2 - 3, w / 3 + step * 7, h / 2 + 3, context);
    canvas_arrow(context, w / 3, h / 9 + 1, w / 3, h / 9);
    canvas_arrow(context, w - w / 9, h / 2, w - w / 9, h / 2);
    for (var k = 0; k < xp.length - 1; k++) {
        context.beginPath();
        context.moveTo(xp[k], yp[k]);
        context.lineTo(xp[k + 1], yp[k + 1]);
        context.stroke();
    }
    drawCircle(particle, context, 'black');
    timeShow = Number((((particle.x - (w / 3)) / step) / v) * Math.pow(10, 6)).toFixed(2);

    context.fillStyle = "rgba(255,255,255, 0.5)";
    context.beginPath();
    context.fillRect(0, 240, 200, 150);
    context.beginPath();
    context.fillRect(410, 35, 250, 75);
    context.fillStyle = 'black';
    context.fillText("x= " + Number((particle.x - (w / 3)) / step).toFixed(2) + " см", 20, 270);
    context.fillText("E'= " + Number(E * 1000 * Eres).toFixed(2) + " В", 420, 100);
    context.fillText("y= " + (-1) * Number((particle.y - (h / 2)) / step).toFixed(2) + " см", 20, 290);
    context.fillText("t= " + timeShow + "*10\u207B\u2078" + " с", 20, 310);
    context.fillText("v\u2093 " + " = " + vx + "*10\u2076" + " м/с", 20, 340);
    context.fillText("vᵧ= " + Number((-a * timeShow) / Math.pow(10, 14)).toFixed(2) + "*10\u2076" + " м/с", 20, 360);

    context.fillText("q = -1,60*10\u207B\u00B9\u2079" + " Кл", 420, 60);
    context.fillText("m = 9,11*10\u207B\u00B3\u00B9" + " кг", 420, 78);

    context.fillText("y", 250, 70);
    context.fillText("x", w - w / 7, h / 2 + 20);
    context.fillText("7 см", w / 3 + step * 7, h / 2 + 20);

    requestAnimFrame(function () {
        if (doAnim) {
            animateThreads(particle, canvas, context, i, x1);
        } else {
            context = null;
            return;
        }
    });

}

function animateSimple(particle, canvas, context, i) {
    var vx = $("#v0x").val();
    var E = $("#e").val();
    var a = -1.76 * Math.pow(10, 11) * E * 1000;
    var v = vx * Math.pow(10, 6);
    var tS = 0.01 / v;
    var g = 9.81;

    i = i + 0.00000000001;


    if ((particle.y <= h / 2 + step - particle.radius) && (particle.x <= w - step * 7 + particle.radius)) {
        xp.push(particle.x);
        yp.push(particle.y);
        particle.x = (v * i) * 100 / 37.795 + (particle.x - w / 3) + w / 3;
        particle.y = h / 2 + (particle.y - h / 2) - (((a * Math.pow(i, 2)) / 2)) * 100 / 37.95;

        //console.log(   particle.x+"  "+ particle.y)
    } else if (particle.x >= w - step * 7 + particle.radius) {
        xp.push(particle.x);
        yp.push(particle.y);
        particle.x = (v * i) * 100 / 37.795 + (particle.x - w / 3) + w / 3;
        particle.y = h / 2 + (particle.y - h / 2) - (((g * Math.pow(i, 2)) / 2)) * 100 / 37.95;
    } else {
        doAnim = false;
    }

    context.clearRect(0, 0, canvas.width, canvas.height);
    context.drawImage(background, 0, 0);
    context.drawImage(ob1, w / 3, h / 2 + step);
    context.drawImage(ob2, w / 3, h / 2 - step - 20);
    context.font = "18px Times";


    drawLine(w / 9, h / 2, w - w / 9, h / 2, context);
    drawLine(w / 3, h / 9, w / 3, h - h / 6, context);
    drawLine(w / 3 + step * 7, h / 2 - 3, w / 3 + step * 7, h / 2 + 3, context);
    canvas_arrow(context, w / 3, h / 9 + 1, w / 3, h / 9);
    canvas_arrow(context, w - w / 9, h / 2, w - w / 9, h / 2);
    for (var k = 0; k < xp.length - 1; k++) {
        context.beginPath();
        context.moveTo(xp[k], yp[k]);
        context.lineTo(xp[k + 1], yp[k + 1]);
        context.stroke();
    }

    timeShow = Number((((particle.x - (w / 3)) / step) / v) * Math.pow(10, 6)).toFixed(2);

    context.fillStyle = "rgba(255,255,255, 0.5)";
    context.beginPath();
    context.fillRect(0, 240, 200, 150);
    context.beginPath();
    context.fillRect(410, 25, 200, 65);
    context.fillStyle = 'black';
    context.fillText("x= " + Number((particle.x - (w / 3)) / step).toFixed(2) + " см", 20, 270);
    context.fillText("y= " + (-1) * Number((particle.y - (h / 2)) / step).toFixed(2) + " см", 20, 290);
    context.fillText("t= " + timeShow + "*10\u207B\u2078" + " с", 20, 310);
    context.fillText("v\u2093 " + " = " + vx + "*10\u2076" + " м/с", 20, 340);
    context.fillText("vᵧ= " + Number((-a * timeShow) / Math.pow(10, 14)).toFixed(2) + "*10\u2076" + " м/с", 20, 360);
    decorate();
    drawCircle(particle, context, 'black');
    requestAnimFrame(function () {
        if (doAnim) {

            animateSimple(particle, canvas, context, i);
        } else {
            context = null;
            return;
        }
    });

}

var chargeOne = {
    x: w / 2,
    y: h / 2 - step - 20 - step * 4,
    radius: 8
};
var chargeTwo = {
    x: w / 2,
    y: h / 2 + step + 20 + step * 4,
    radius: 8
};
var particle = {
    x: w / 3,
    y: h / 2,
    radius: 4
};


var i = 0;
startButton.click(function () {
    if (!doAnim) {
        doAnim = true;
        context = canvas.getContext('2d');
    }
    i = 0;
    if ($("#type").val() === '1') {
        // startTime = (new Date()).getTime()/1000;
        animateSimple(particle, canvas, context, i);
    } else if ($("#type").val() === '2') {
        var x1 = 0.0005;
        particle.y = h / 2;
        particle.x = w / 3;

        animateThreads(particle, canvas, context, i, x1);
    } else {
        animateCharges(particle, canvas, context, i);
    }
    ;
});
stopButton.click(function () {
    doAnim = false;
});

refreshButton.click(function () {
    context = canvas.getContext('2d');
    particle.y = h / 2;
    particle.x = w / 3;
    xp = [];
    xy = [];
    i = 0;

})
