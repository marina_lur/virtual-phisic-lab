var $table = $('#myTable');

$(function () {
    $('.btn-primary').not('#create').addClass('disabled');
    $table.on('click-row.bs.table', function (e, row, $element) {
        if ($($element).hasClass('info')) {
            $('.info').removeClass('info');
            $('.btn-primary').not('#create').addClass('disabled');
        } else {
            $($element).addClass('info').siblings().removeClass('info');
            $('.btn-primary').removeClass('disabled');
        }

    });

});