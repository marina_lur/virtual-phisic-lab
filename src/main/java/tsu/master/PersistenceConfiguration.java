package tsu.master;

import lombok.val;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@EnableTransactionManagement
@SpringBootConfiguration
public class PersistenceConfiguration {

    @Bean(name = "entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {
        val emf = new LocalContainerEntityManagerFactoryBean();
        emf.setPersistenceUnitName(PERSISTENCE_UNIT);
        emf.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        emf.setPackagesToScan("tsu.master");
        emf.setDataSource(dataSource);
        Map<String, String> properties = new HashMap<>();
        properties.put("hibernate.show_sql", "true");
        properties.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
        emf.setJpaPropertyMap(properties);
        return emf;
    }
    @Bean
    public DataSource dataSource() {
        val dataSource = new DriverManagerDataSource();
        //  dataSource.setUrl("jdbc:postgresql://localhost:5432/vpl_database");
        dataSource.setUrl("jdbc:postgresql://192.168.68.8:5433/vpl_database");
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUsername("system");
        dataSource.setPassword("system");
        return dataSource;
    }

    @Bean(name = "transactionManager")
    public PlatformTransactionManager transactionManager(EntityManagerFactory factory) {
        val tm = new JpaTransactionManager();
        tm.setEntityManagerFactory(factory);
        return tm;
    }


    private static final Logger log = LoggerFactory.getLogger(PersistenceConfiguration.class);
    private static final String PERSISTENCE_UNIT = "VPL_Persistence_Unit";
}