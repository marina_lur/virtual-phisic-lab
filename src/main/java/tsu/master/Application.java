package tsu.master;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.Arrays;

@SuppressWarnings("SpringFacetCodeInspection")
@SpringBootApplication
@EnableTransactionManagement
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class Application {

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(Application.class, args);

        System.out.println("Let's inspect the beans provided by Spring Boot:");

        Arrays.stream(ctx.getBeanDefinitionNames())
                .sorted()
                .forEach(log::info);
    }

    static Logger log = LoggerFactory.getLogger(Application.class);
}
