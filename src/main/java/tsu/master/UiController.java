package tsu.master;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import tsu.master.persistence.DepartmentRepository;
import tsu.master.persistence.GroupRepository;
import tsu.master.persistence.PositionRepository;
import tsu.master.persistence.TestRepository;
import tsu.master.persistence.UserRepository;
import tsu.master.persistence.UserTestRepository;


@Controller
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class UiController {
    @Autowired

    public UiController(TestRepository testRepository, UserRepository userRepository, UserTestRepository userTestRepository, PositionRepository positionRepository, GroupRepository groupRepository, DepartmentRepository departmentRepository) {
        this.testRepository = testRepository;
        this.userTestRepository = userTestRepository;

        this.positionRepository = positionRepository;
        this.groupRepository=groupRepository;

        this.userRepository=userRepository;
        this.departmentRepository = departmentRepository;
    }

    @RequestMapping(value = "/users")
    public String students(Model model){
        model.addAttribute("users", userRepository.findAll());
        model.addAttribute("groups",groupRepository.findAll());
        model.addAttribute("positions", positionRepository.findAll());
        model.addAttribute("departments", departmentRepository.findAll());
        return "users";
    }


    @RequestMapping(value = "/greeting")
    public String greeting(@RequestParam(value="name", required=false, defaultValue="World") String name, Model model) {
        model.addAttribute("name", name);
        return "greeting";
    }

    @RequestMapping("/groups")
    public String groups(Model model)  {
        ;
        model.addAttribute("departments", departmentRepository.findAll());
        model.addAttribute("groups", groupRepository.findAll());

        return "groups";
    }
    @RequestMapping("/departments")
    public String departments(Model model)  {
        model.addAttribute("departments", departmentRepository.findAll());
        return "departments";
    }

    @RequestMapping("/positions")
    public String positions(Model model)  {
        model.addAttribute("positions", positionRepository.findAll());
        return "positions";
    }

    @RequestMapping("/tests")
    public String tests(Model model) {
        model.addAttribute("tests", testRepository.findAll());
        return "tests";
    }

    @RequestMapping("/usersTests")
    public String usersTests(Model model) {
        model.addAttribute("userTests", userTestRepository.findAll());
        return "usersTests";
    }

    @RequestMapping("/lab")
    public String lab(Model model) {
        return "lab";
    }

    TestRepository testRepository;
    UserTestRepository userTestRepository;
    PositionRepository positionRepository;
    DepartmentRepository departmentRepository;
    UserRepository userRepository;
    GroupRepository groupRepository;
    static Logger log = LoggerFactory.getLogger(UiController.class);
}
