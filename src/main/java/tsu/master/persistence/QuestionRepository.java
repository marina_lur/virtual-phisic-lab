package tsu.master.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import tsu.master.entities.Question;

import java.math.BigInteger;

@RepositoryRestResource(collectionResourceRel = "question", path = "question")
public interface QuestionRepository extends JpaRepository<Question, BigInteger> {
}
