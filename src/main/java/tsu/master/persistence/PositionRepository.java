package tsu.master.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import tsu.master.entities.Position;

import java.math.BigInteger;

@RepositoryRestResource(collectionResourceRel = "position", path = "position")
public interface PositionRepository extends JpaRepository<Position, BigInteger> {
}
