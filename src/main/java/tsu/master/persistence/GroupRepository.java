package tsu.master.persistence;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import tsu.master.entities.Department;
import tsu.master.entities.Group;


import java.math.BigInteger;

@RepositoryRestResource(collectionResourceRel = "group", path = "group")
public interface GroupRepository  extends JpaRepository<Group, BigInteger> {
    Page<Group> findAll(Pageable pageable);


}