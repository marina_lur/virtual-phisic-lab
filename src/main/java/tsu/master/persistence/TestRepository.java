package tsu.master.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import tsu.master.entities.Test;

import java.math.BigInteger;

@RepositoryRestResource(collectionResourceRel = "test", path = "test")
public interface TestRepository extends JpaRepository<Test, BigInteger> {
}
