package tsu.master.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import tsu.master.entities.UserTest;

import java.math.BigInteger;

@RepositoryRestResource(collectionResourceRel = "user_test", path = "user_test")
public interface UserTestRepository extends JpaRepository<UserTest, BigInteger> {
}
