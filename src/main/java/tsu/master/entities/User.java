package tsu.master.entities;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigInteger;

@Data
@Entity(name = "users")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    BigInteger userId;

    @NotNull
    @Size(min = 2, max = 400)
    @Column(name = "name", nullable = false)
    String name;

    @NotNull
    @Size(min = 2, max = 400)
    @Column(name = "surname", nullable = false)
    String surname;

    @NotNull
    @Size(min = 2, max = 200)
    @Column(name = "login", nullable = false)
    String login;

    @Size(min = 2, max = 400)
    @Column(name = "middlename")
    String middlename;

    @Column(name = "role", nullable = false)
    String role;

    @ManyToOne
    @JoinColumn(name = "group_id")
    Group group;

    @ManyToOne
    @JoinColumn(name = "department_id")
    Department department;

    @ManyToOne
    @JoinColumn(name = "position_id")
    Position position;

    @Column(name = "hash")
    String hash;

    @Column(name = "salt")
    String salt;

    private static final Logger log = LoggerFactory.getLogger(User.class);
}