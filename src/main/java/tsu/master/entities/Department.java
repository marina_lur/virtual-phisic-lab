package tsu.master.entities;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigInteger;

@SuppressWarnings("JpaDataSourceORMInspection")
@Data
@Entity(name = "departments")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Department implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "department_id")
    BigInteger departmentId;

    @Size(min = 2, max = 400)
    @NotNull
    @Column(name = "department_name")
    String departmentName;

    private static final Logger log = LoggerFactory.getLogger(Department.class);
}