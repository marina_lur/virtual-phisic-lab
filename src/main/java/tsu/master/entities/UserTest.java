package tsu.master.entities;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.math.BigInteger;


@Data
@Entity(name = "users_tests")

@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserTest implements Serializable {

    @Id
    BigInteger id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    User user;

    @ManyToOne
    @JoinColumn(name = "test_id")
    Test test;

    private static final Logger log = LoggerFactory.getLogger(UserTest.class);

}