package tsu.master.entities;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigInteger;


@SuppressWarnings("JpaDataSourceORMInspection")
@Data
@Entity(name = "groups")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Group {
    public Group() {
    }

    protected boolean canEqual(Object other) {
        return other instanceof Group;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "group_id")
    BigInteger groupId;

    @Size(min = 2, max = 400)
    @NotNull
    @Column(name = "group_name", nullable = false)
    String groupName;

    @ManyToOne
    @JoinColumn(name = "department_id",nullable = true)
    Department department;

    private static final Logger log = LoggerFactory.getLogger(Group.class);
}